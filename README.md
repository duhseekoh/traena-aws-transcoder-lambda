## Overview
AWS Lambda - Transcode video files from an s3 bucket, and put an HLS stream in another s3 bucket.

Given an ElasticTranscoder PipelineId, and files dumped into the pipleline's associated
S3 input bucket, this lambda will take each video input file and output
(into pipeline's associated output bucket) an HLS video stream.

This currently is setup for Traena. By setting up environment variables for the
pipeline and video presets, it would become generic.

## Output
HLS stream with the following encodings:
- Video @ 128kbps
- Video @ 400kbps
- Video @ 600kbps
- Audio @ 64kbps

## TODO
- [x] environment variables for video preset ids
- [x] environment variable for PipelineId
- [x] thumbnail creation
- [ ] change bitrate numbers into quality words (e.g. golf-128.ts would be golf-low.ts)
