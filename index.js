"use strict";
var AWS = require('aws-sdk');

// var sampleEvent = {
//     "Records": [
//         {
//             "eventVersion": "2.0",
//             "eventSource": "aws:s3",
//             "awsRegion": "us-west-2",
//             "eventTime": "2016-12-09T16:05:48.292Z",
//             "eventName": "ObjectCreated:Put",
//             "userIdentity": {
//                 "principalId": "AWS:AIDAIG7JIWBNELJUVJSRU"
//             },
//             "requestParameters": {
//                 "sourceIPAddress": "98.206.240.196"
//             },
//             "responseElements": {
//                 "x-amz-request-id": "9B37B54FFEAC8613",
//                 "x-amz-id-2": "ZFmIkqDeBcGaJ9n8fjEEJyh+EqCIw4oILiGbKO8BW7Avqt7Illg5fIQ7/1WpEYak"
//             },
//             "s3": {
//                 "s3SchemaVersion": "1.0",
//                 "configurationId": "a8296926-c3f6-4d68-9d4a-ab8d932b1015",
//                 "bucket": {
//                     "name": "prototype-video-input",
//                     "ownerIdentity": {
//                         "principalId": "AZZP94PBHPEOY"
//                     },
//                     "arn": "arn:aws:s3:::prototype-video-input"
//                 },
//                 "object": {
//                     "key": "golf.mp4",
//                     "size": 1279450,
//                     "eTag": "d0a5f01924ee4429916ccc2a0684630b",
//                     "sequencer": "00584AD65C23C5EE83"
//                 }
//             }
//         }
//     ]
// }

exports.handler = (event, context, lambdaCallback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    // Parse filename
    let inputKey = event.key || event.Records[0].s3.object.key;
    let outputThumbnailsPath = inputKey.replace(/(video-input\/.+)/, 'images');
    let outputVideoPath = inputKey.replace(/(video-input\/.+)/, 'video-output');
    let outputSegmentDuration = '10';

    // Grab env vars
    let PIPELINE_ID = process.env.PIPELINE_ID,
      VIDEO_PRESET_128_ID = process.env.VIDEO_PRESET_128_ID,
      VIDEO_PRESET_400_ID = process.env.VIDEO_PRESET_400_ID,
      VIDEO_PRESET_600_ID = process.env.VIDEO_PRESET_600_ID,
      VIDEO_PRESET_3000_ID = process.env.VIDEO_PRESET_3000_ID,
      AUDIO_PRESET_64_ID = process.env.AUDIO_PRESET_64_ID;

    // Validate env vars
    if(!PIPELINE_ID || !VIDEO_PRESET_128_ID || !VIDEO_PRESET_400_ID ||
      !VIDEO_PRESET_600_ID || !VIDEO_PRESET_3000_ID || !AUDIO_PRESET_64_ID)
    {
      lambdaCallback('Error: Missing env vars for pipeline or preset id(s)');
      return;
    }

    var elastictranscoder = new AWS.ElasticTranscoder();
    var params = {
      PipelineId: PIPELINE_ID,
      Input: {
        AspectRatio: 'auto',
        Container: 'auto',
        FrameRate: 'auto',
        Interlaced: 'auto',
        Key: inputKey,
        Resolution: 'auto',
      },
      Outputs: [
        {
          Key: `${outputVideoPath}/video-128`,
          PresetId: VIDEO_PRESET_128_ID,
          Rotate: 'auto',
          SegmentDuration: outputSegmentDuration,
        },
        {
          Key: `${outputVideoPath}/video-400`,
          PresetId: VIDEO_PRESET_400_ID,
          Rotate: 'auto',
          SegmentDuration: outputSegmentDuration,
          ThumbnailPattern: `${outputThumbnailsPath}/thumbnail-{count}`,
        },
        {
          Key: `${outputVideoPath}/video-600`,
          PresetId: VIDEO_PRESET_600_ID,
          Rotate: 'auto',
          SegmentDuration: outputSegmentDuration,
        },
        {
          Key: `${outputVideoPath}/video-3000`,
          PresetId: VIDEO_PRESET_3000_ID,
          Rotate: 'auto',
          SegmentDuration: outputSegmentDuration,
        },
        {
          Key: `${outputVideoPath}/video-audio-64`,
          PresetId: AUDIO_PRESET_64_ID,
          SegmentDuration: outputSegmentDuration,
        },
        /* more items */
      ],
      Playlists: [
        {
          Format: 'HLSv4',
          Name: `${outputVideoPath}/video`,
          OutputKeys: [
            // default to a decent quality so video players that start with the
            // first in the playlist dont get the lowest quality.
            `${outputVideoPath}/video-600`,
            `${outputVideoPath}/video-128`,
            `${outputVideoPath}/video-400`,
            `${outputVideoPath}/video-3000`,
            `${outputVideoPath}/video-audio-64`,
          ]
        },
        /* more items */
      ],
      UserMetadata: {
        testMetadata: 'Test metadata',
        /* anotherKey: ... */
      }
    };
    elastictranscoder.createJob(params, function(err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
        lambdaCallback('Error creating job');
      } else {
        console.log(data);           // successful response
        lambdaCallback(null, data);
      }
    });
};
